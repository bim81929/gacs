from WCE import WCE


class Sensor:
    E_MIN = 540
    E_MAX = 10800

    def __init__(self, xCorr, yCorr, p, E):
        """
            xCorr: Toạ độ X
            yCorr: Toạ độ Y
            p    : Công suất tiêu thụ của Sensor
            E    : Năng lượng còn lại của Sensor
        """
        self.xCorr = xCorr
        self.yCorr = yCorr
        self.p = p
        self.E = E
        self.tMax = (1.0 * (Sensor.E_MAX - Sensor.E_MIN)) / (WCE.U - self.p)

