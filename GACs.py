import math
import random

import matplotlib.pyplot as plt
import numpy as np

from Constant import Constant
from Population import Population
from Sensor import Sensor
from WCE import WCE


class Individual:
    def __init__(self, path):
        self.path = path
        #         self.pathCost = self.calculatePathCost()
        self.pathTaus = []
        self.fitnessF = self.calculateFitnessF()
        self.fitnessG = 0

        # Trạng thái của các sensors có index trong self.path
        # True: Sống, False: chết
        self.pathState = []
        self.eAtEnd = []  # Danh sách NL tương ứng lúc kết thúc vòng sạc

    def calculatePathCost(self):
        pass

    def calculateWaitingTimesByTravel(self):
        """
            RETURN: Giá trị thời gian chờ của các sensor (Thời gian thăm tích luỹ)
        """
        waitingTimes = []
        for i, idx in enumerate(self.path):
            if i == 0:
                waitingTime = (1.0 * Map.calculateDistance(-2, idx)) / WCE.V
            else:
                waitingTime = waitingTimes[i - 1] + (1.0 * Map.calculateDistance(self.path[i - 1], idx)) / WCE.V

            waitingTimes.append(waitingTime)

        return waitingTimes

    def calculateWaitingTimesByCharging(self):
        """
            RETURN: Giá trị thời gian chờ của các sensor (Thời gian sạc tích luỹ)
        """
        waitingTimes = []
        for i in range(len(self.pathTaus)):
            if i == 0:
                waitingTime = 0
            else:
                waitingTime = waitingTimes[i - 1] + self.pathTaus[i - 1]

            waitingTimes.append(waitingTime)

        return waitingTimes

    def calculateSensorWeight(self, sensor):
        return (1.0 * sensor.E) / sensor.p

    def calculateFitnessF(self):
        sensors = list(np.array(Map.sensors)[[self.path]])
        sensorWeights = [self.calculateSensorWeight(sensor) for sensor in sensors]
        waitingTimes = self.calculateWaitingTimesByTravel()

        items = np.array(waitingTimes) / np.array(sensorWeights)
        f1 = np.sum(items)
        avgf1 = f1 / len(self.path)
        f2 = np.sum(np.abs(items - avgf1))
        f = Constant.ALPHA * f1 + (1 - Constant.ALPHA) * f2

        return f

    def calculateFitnessG(self):

        self.fitnessG = 0
        self.pathState = []
        self.eAtEnd = []
        for i, idx in enumerate(self.path):
            sensor = Map.sensors[idx]
            waitingTimesByTravel = self.calculateWaitingTimesByTravel()
            waitingTimesByCharging = self.calculateWaitingTimesByCharging()

            # Năng lượng còn lại lúc đến thăm sensor
            eAtSensor = sensor.E - (waitingTimesByTravel[i] + waitingTimesByCharging[i]) * sensor.p
            # Năng lượng còn lại lúc MC hoàn thành round K
            waitingTimesByTravelEnd = waitingTimesByTravel[-1] + 1.0 * Map.calculateDistance(self.path[-1], -2) / WCE.V
            eAtEnd = sensor.E + self.pathTaus[i] * WCE.U - (waitingTimesByTravelEnd + sum(self.pathTaus)) * sensor.p
            self.eAtEnd.append(eAtEnd)

            if eAtSensor < Sensor.E_MIN or eAtEnd < Sensor.E_MIN:
                self.fitnessG += 1
                self.pathState.append(False)
            else:
                self.pathState.append(True)

    def updateEnergy(self):
        # Update lại indexList của mạng
        Map.indexList = []
        for i, value in enumerate(self.pathState):
            Map.sensors[self.path[i]].E = self.eAtEnd[i]
            if value:
                Map.indexList.append(self.path[i])


class Map:
    BASE_STATION = ""
    sensors = []
    indexList = []  # Danh sách các index sensors sống.
    wce = ""

    def __init__(self, sensors, wce):
        """
            sensors : danh sách các Sensor
            wce     : Xe sạc
        """
        Map.BASE_STATION = Sensor(Constant.DEFAULT_X, Constant.DEFAULT_Y, 0, 0)
        Map.sensors = sensors
        Map.indexList = np.arange(0, len(sensors))
        Map.wce = wce
        self.wceThreshold = 100

    @staticmethod
    def calculateDistance(idx1, idx2):
        """
            idx:
                -1 : base station
                -2 : wce
                else: sensors
        """
        if idx1 == -1:
            sensor1 = Map.BASE_STATION
        elif idx1 == -2:
            sensor1 = wce
        else:
            sensor1 = Map.sensors[idx1]

        if idx2 == -1:
            sensor2 = Map.BASE_STATION
        elif idx2 == -2:
            sensor2 = wce
        else:
            sensor2 = Map.sensors[idx2]

        x_diff = float(sensor1.xCorr) - float(sensor2.xCorr)
        y_diff = float(sensor1.yCorr) - float(sensor2.yCorr)

        distance = math.sqrt(x_diff * x_diff + y_diff * y_diff)
        return distance


def initPopulation1(map, nIndividuals):
    individuals = []
    checked = []
    path = Map.indexList

    for i in range(Constant.POPULATION_SIZE):
        print("Khởi tạo cá thể thứ ", i, end='\r')
        while True:
            # Danh sách các index của sensors sống
            path = list(np.random.permutation(path))
            if path not in checked:
                checked.append(path)
                break
        individual = Individual(path)
        individuals.append(individual)

    Population.individuals = individuals
    # Sắp xếp quần thể theo fitnessF
    Population.individuals.sort(key=lambda individual: individual.fitnessF)


def recombine1(parentIndex1, parentIndex2, method=0):
    """
        Phương pháp lai ghép:
            - spx: 0
            - pmx: 1
    """
    parentPath1 = Population.individuals[parentIndex1].path
    parentPath2 = Population.individuals[parentIndex2].path
    nGens = len(parentPath1)

    childPath1 = []
    childPath2 = []

    if method == 0:
        crossoverPoint = random.randint(1, nGens - 1)

        childPath1 = parentPath1[:crossoverPoint]
        childPath2 = parentPath2[:crossoverPoint]

        remain1 = [x for x in parentPath2 if x not in childPath1]
        remain2 = [x for x in parentPath1 if x not in childPath2]

        childPath1 = childPath1 + remain1
        childPath2 = childPath2 + remain2

        offSpring1 = Individual(childPath1)
        offSpring2 = Individual(childPath2)

        return (offSpring1, offSpring2)
    else:
        crossoverPoint1 = random.randint(1, nGens - 2)
        crossoverPoint2 = random.randint(crossoverPoint1 + 1, nGens - 1)

        childPath1 = np.array([-1] * nGens)
        childPath2 = np.array([-1] * nGens)

        middle1 = parentPath2[crossoverPoint1: crossoverPoint2]
        middle2 = parentPath1[crossoverPoint1: crossoverPoint2]

        childPath1[crossoverPoint1: crossoverPoint2] = middle1
        childPath2[crossoverPoint1: crossoverPoint2] = middle2

        # Xây dựng từ điển để tra các gen đã có trong middle
        searchItem1 = dict(zip(middle1, middle2))  # parent2Item : key, parent1Item: value
        searchItem2 = dict(zip(middle2, middle1))  # parent1Item : key, parent2Item: value

        for i in range(nGens):
            if i >= crossoverPoint1 and i < crossoverPoint2:
                continue
            gen1 = parentPath1[i]
            gen2 = parentPath2[i]

            if gen1 not in middle1:
                childPath1[i] = gen1
            else:
                while (True):
                    value = searchItem1[gen1]
                    if value not in middle1:
                        childPath1[i] = value
                        break
                    else:
                        gen1 = value

            if gen2 not in middle2:
                childPath2[i] = gen2
            else:
                while (True):
                    value = searchItem2[gen2]
                    if value not in middle2:
                        childPath2[i] = value
                        break
                    else:
                        gen2 = value

        offSpring1 = Individual(list(childPath1))
        offSpring2 = Individual(list(childPath2))

        return (offSpring1, offSpring2)


def mutate1(offSpring, method=0):
    """
        Phương pháp đột biến:
            - cim: 0
            - swap: 1

        Note: Khi thay đổi hành trình thì ta phải tính lại fitnessF cho offSpring
    """
    offSpringPath = offSpring.path
    nGens = len(offSpringPath)

    if method == 0:
        mutationPoint = random.randint(1, nGens - 1)
        offSpringPath[:mutationPoint] = offSpringPath[:mutationPoint][::-1]
        offSpringPath[mutationPoint:] = offSpringPath[mutationPoint:][::-1]
    else:
        mutationPoint1 = random.randint(0, nGens - 2)
        mutationPoint2 = random.randint(mutationPoint1 + 1, nGens - 1)

        temp = offSpringPath[mutationPoint1]
        offSpringPath[mutationPoint1] = offSpringPath[mutationPoint2]
        offSpringPath[mutationPoint2] = temp

    offSpring.path = offSpringPath
    offSpring.fitnessF = offSpring.calculateFitnessF()


def selectSurvivor1(offSprings):
    individuals = Population.individuals
    offSprings.sort(key=lambda offSpring: offSpring.fitnessF)

    newIndividuals = []
    idx1 = 0
    idx2 = 0

    while len(newIndividuals) <= Constant.POPULATION_SIZE:
        if individuals[idx1].fitnessF <= offSprings[idx2].fitnessF:
            newIndividuals.append(individuals[idx1])
            idx1 += 1
        else:
            newIndividuals.append(offSprings[idx2])
            idx2 += 1
    # Cập nhật quần thể mới
    Population.individuals = newIndividuals


def phase_1():
    ####### Thuật Toán GACs #######
    ###     Pha 1: Tìm hành trình tối ưu     ###

    bestFitnessOver = []
    ### Khởi tạo quần thể, tính fitnessF ###
    initPopulation1(map, Constant.POPULATION_SIZE)
    ### Thực thi qua các thế hệ ###

    for generation in range(Constant.N_GENERATIONS_1):
        bestFitnessOver.append(Population.individuals[0].fitnessF)

        ### Lai ghép, đột biến ###
        offSprings = []
        for i in range(Constant.POPULATION_SIZE):
            # Sinh giá trị ngẫu nhiên lai ghép
            rmx = random.random()
            if rmx < Constant.CROSSOVER_RATE:
                # Sinh index của 2 cha mẹ
                parentIndex1 = random.randint(0, Constant.POPULATION_SIZE - 1)
                while True:
                    parentIndex2 = random.randint(0, Constant.POPULATION_SIZE - 1)
                    if parentIndex2 != parentIndex1:
                        break
                # Lựa chọn phương pháp lai ghép và lai ghép 2 cha mẹ
                rmpmx = random.random()
                if rmpmx < Constant.SPX_RATE:
                    offSpring1, offSpring2 = recombine1(parentIndex1, parentIndex2, 0)
                else:
                    offSpring1, offSpring2 = recombine1(parentIndex1, parentIndex2, 1)
                # Lựa chọn phương pháp đột biến và đột biến các cá thể con
                # Con 1
                rmp = random.random()
                if rmp < Constant.MUTATION_RATE:
                    rmcim = random.random()
                    if rmcim < Constant.CIM_RATE:
                        mutate1(offSpring1, 0)
                    else:
                        mutate1(offSpring1, 1)

                # Con 2
                rmp = random.random()
                if rmp < Constant.MUTATION_RATE:
                    rmcim = random.random()
                    if rmcim < Constant.CIM_RATE:
                        mutate1(offSpring2, 0)
                    else:
                        mutate1(offSpring2, 1)

                offSprings.append(offSpring1)
                offSprings.append(offSpring2)

                # Chỉ sinh không quá Số lượng quần thể gốc
                if (len(offSprings) >= Constant.POPULATION_SIZE):
                    break

        ### Chọn lọc cá thể ###
        selectSurvivor1(offSprings)
    plt.plot(np.arange(Constant.N_GENERATIONS_1), bestFitnessOver)


def best_path():
    ###     Pha 2: Tìm thời gian sạc tối ưu     ###

    # Gán lại hành trình tốt nhất cho toàn quần thể
    bestPath = Population.individuals[0].path
    for individual in Population.individuals:
        individual.path = bestPath

    # Tính năng lượng di chuyển của MC theo hành trình tốt nhất từ Pha 1
    travelEnergy = 0

    for i in range(len(bestPath)):
        if i == 0:
            travelEnergy += (1.0 * (Map.calculateDistance(bestPath[i], -2) * WCE.P_M)) / WCE.V
        elif i == (len(bestPath) - 1):
            travelEnergy += (1.0 * (Map.calculateDistance(bestPath[i], bestPath[i - 1]) * WCE.P_M)) / WCE.V
            travelEnergy += (1.0 * (Map.calculateDistance(-2, bestPath[i]) * WCE.P_M)) / WCE.V
        else:
            travelEnergy += (1.0 * (Map.calculateDistance(bestPath[i], bestPath[i - 1]) * WCE.P_M)) / WCE.V

    Map.wce.travelEnergy = travelEnergy

    # Tính tổng thời gian để sạc cho các cảm biến
    # Map.wce.chargingTimeTotal = (1.0 * (WCE.E_MC - travelEnergy)) / WCE.U
    Map.wce.chargingTimeTotal = Constant.T - Map.wce.travelEnergy


### Khởi tạo quần thể, tính fitnessG ###
def initPopulation2():
    chargingTimeTotal = Map.wce.chargingTimeTotal
    for j, individual in enumerate(Population.individuals):
        nGens = len(individual.path)
        remainTime = chargingTimeTotal
        pathTaus = [0] * nGens
        notCharged = individual.path

        pathIndex = list(range(nGens))
        random.shuffle(pathIndex)  # Lấy hoán vị của danh sách index ban đầu của path cho nhanh =)

        for i in pathIndex:
            sensorIndex = individual.path[i]
            sensor = Map.sensors[sensorIndex]

            rmTime = random.uniform(0, min(1.0 * remainTime / chargingTimeTotal, 1.0 * sensor.tMax / chargingTimeTotal))
            pathTaus[i] = rmTime * chargingTimeTotal
            remainTime -= pathTaus[i]

        i = 0
        while remainTime:
            sensorIndex = individual.path[i]
            sensor = Map.sensors[sensorIndex]
            if remainTime < (sensor.tMax - pathTaus[i]):
                pathTaus[i] += remainTime
                remainTime = 0
                break
            else:
                rmAddTime = random.uniform(0, min(1.0 * remainTime / chargingTimeTotal,
                                                  1.0 * (sensor.tMax - pathTaus[i]) / chargingTimeTotal))
                pathTaus[i] += rmAddTime * chargingTimeTotal
                remainTime -= rmAddTime * chargingTimeTotal
                i += 1

                if i == nGens:
                    i = 0

        individual.pathTaus = pathTaus
        individual.calculateFitnessG()

    # Sắp xếp các cá thể trong quần thể theo thứ tự tăng dần của fitnessG
    Population.individuals.sort(key=lambda individual: individual.fitnessG)


def recombine2(parentIndex1, parentIndex2):
    #     Lai ghép 2 cha mẹ theo phương pháp SPAH (Single-Point + AMXO)

    parentTaus1 = Population.individuals[parentIndex1].pathTaus
    parentTaus2 = Population.individuals[parentIndex2].pathTaus
    parentPath = Population.individuals[0].path
    nGens = len(parentPath)

    crossoverPoint = random.randint(0, nGens - 2)
    offSpringTaus1 = [0] * nGens
    offSpringTaus2 = [0] * nGens

    rmBeta = random.random() * 1 - 0.5
    offSpringTaus1[crossoverPoint] = (
                                             1 - rmBeta) * parentTaus1[crossoverPoint] + rmBeta * parentTaus2[
                                         crossoverPoint]
    offSpringTaus2[crossoverPoint] = rmBeta * parentTaus1[crossoverPoint] + \
                                     (1 - rmBeta) * parentTaus2[crossoverPoint]

    offSpringTaus1[:crossoverPoint] = parentTaus1[:crossoverPoint]
    offSpringTaus2[:crossoverPoint] = parentTaus2[:crossoverPoint]

    offSpringTaus1[crossoverPoint + 1:] = parentTaus2[crossoverPoint + 1:]
    offSpringTaus2[crossoverPoint + 1:] = parentTaus1[crossoverPoint + 1:]

    offSpringTausTotal1 = sum(offSpringTaus1)
    offSpringTausTotal2 = sum(offSpringTaus2)

    if offSpringTausTotal1 > Map.wce.chargingTimeTotal:
        temp = 1.0 * offSpringTausTotal1 / Map.wce.chargingTimeTotal
        offSpringTaus1 = [x / temp for x in offSpringTaus1]

    elif offSpringTausTotal1 < Map.wce.chargingTimeTotal:
        remainTotal = Map.wce.chargingTimeTotal - offSpringTausTotal1
        remainTime = remainTotal

        i = 0
        while remainTime:
            sensorIndex = parentPath[i]
            sensor = Map.sensors[sensorIndex]
            if remainTime < (sensor.tMax - offSpringTaus1[i]):
                offSpringTaus1[i] += remainTime
                remainTime = 0
                break
            else:
                rmAddTime = random.uniform(0, 1)
                offSpringTaus1[i] += rmAddTime * \
                                     (sensor.tMax - offSpringTaus1[i])
                remainTime -= offSpringTaus1[i]
                i += 1

                if i == nGens:
                    i = 0

    if offSpringTausTotal2 > Map.wce.chargingTimeTotal:
        temp = 1.0 * offSpringTausTotal2 / Map.wce.chargingTimeTotal
        offSpringTaus2 = [x / temp for x in offSpringTaus2]

    elif offSpringTausTotal2 < Map.wce.chargingTimeTotal:
        remainTotal = Map.wce.chargingTimeTotal - offSpringTausTotal2
        remainTime = remainTotal

        i = 0
        while remainTime:
            sensorIndex = parentPath[i]
            sensor = Map.sensors[sensorIndex]
            if remainTime < (sensor.tMax - offSpringTaus2[i]):
                offSpringTaus2[i] += remainTime
                remainTime = 0
                break
            else:
                rmAddTime = random.uniform(
                    0, 1)
                offSpringTaus2[i] += rmAddTime * \
                                     (sensor.tMax - offSpringTaus2[i])
                remainTime -= offSpringTaus2[i]
                i += 1

                if i == nGens:
                    i = 0
    offSpring1 = Individual(parentPath)
    offSpring1.pathTaus = offSpringTaus1
    offSpring1.calculateFitnessG()

    offSpring2 = Individual(parentPath)
    offSpring2.pathTaus = offSpringTaus2
    offSpring2.calculateFitnessG()

    return (offSpring1, offSpring2)


def mutate2(offSpring):
    offSpringTaus = offSpring.pathTaus
    nGens = len(offSpringTaus)

    mutationPoint1 = random.randint(0, nGens - 2)
    mutationPoint2 = random.randint(mutationPoint1 + 1, nGens - 1)

    sensorIndex1 = offSpring.path[mutationPoint1]
    sensor1 = Map.sensors[sensorIndex1]
    theta1 = random.random() * (sensor1.tMax - offSpringTaus[mutationPoint1])
    theta2 = random.random() * offSpringTaus[mutationPoint2]
    theta = min(theta1, theta2)

    offSpringTaus[mutationPoint1] += theta
    offSpringTaus[mutationPoint2] -= theta
    offSpring.pathTaus = offSpringTaus
    offSpring.calculateFitnessG()


def selectSurvivor2(offSprings):
    individuals = Population.individuals
    offSprings.sort(key=lambda offSpring: offSpring.fitnessG)

    newIndividuals = []
    idx1 = 0
    idx2 = 0

    while len(newIndividuals) <= Constant.POPULATION_SIZE:
        if individuals[idx1].fitnessG <= offSprings[idx2].fitnessG:
            newIndividuals.append(individuals[idx1])
            idx1 += 1
        else:
            newIndividuals.append(offSprings[idx2])
            idx2 += 1
    # Cập nhật quần thể mới
    Population.individuals = newIndividuals


def phase_2():
    initPopulation2()
    ### Thực thi qua các thế hệ ###
    bestFitnessOver = []
    bestFitness = -999
    epoch = 0
    for generation in range(Constant.N_GENERATIONS_2):
        if Population.individuals[0].fitnessG == bestFitness:
            epoch += 1
        else:
            epoch = 0
        if epoch == Constant.DEFAULT_STOP_EPOCH:
            break
        bestFitnessOver.append(Population.individuals[0].fitnessG)
        print(
            "[INFO] Số cá thể chết trong thế hệ {} là : {}".format(generation + 1, Population.individuals[0].fitnessG))
        offSprings = []
        for i in range(Constant.POPULATION_SIZE):
            # Sinh giá trị ngẫu nhiên lai ghép
            rmx = random.random()
            if rmx < Constant.CROSSOVER_RATE:
                # Sinh index của 2 cha mẹ
                parentIndex1 = random.randint(0, Constant.POPULATION_SIZE - 1)
                while True:
                    parentIndex2 = random.randint(0, Constant.POPULATION_SIZE - 1)
                    if parentIndex2 != parentIndex1:
                        break
                # Lai ghép 2 cha mẹ
                offSpring1, offSpring2 = recombine2(parentIndex1, parentIndex2)

                # Đột biến các cá thể con
                # Con 1
                rmp = random.random()
                if rmp < Constant.MUTATION_RATE:
                    mutate1(offSpring1)

                # Con 2
                rmp = random.random()
                if rmp < Constant.MUTATION_RATE:
                    mutate1(offSpring2)

                offSprings.append(offSpring1)
                offSprings.append(offSpring2)

                # Chỉ sinh không quá Số lượng quần thể gốc
                if (len(offSprings) >= Constant.POPULATION_SIZE):
                    break

        ### Chọn lọc cá thể ###
        bestFitness = Population.individuals[0].fitnessG
        selectSurvivor2(offSprings)
    plt.plot(np.arange(len(bestFitnessOver)), bestFitnessOver)


def drawl(xlabel, ylabel, name):
    plt.plot(xlabel, ylabel)
    plt.title(name)
    plt.xlabel("Epoch")
    plt.ylabel("Dead sensors")
    plt.show()


if __name__ == '__main__':
    import re
    import warnings

    warnings.filterwarnings("ignore", category=FutureWarning)
    data_folder = "data/"
    file = ['g_100.txt', 'g_200.txt', 'g_250.txt', 'g_300.txt']

    for FILE_NAME in file:
        fileData = open(data_folder + FILE_NAME, 'r')
        inputData = fileData.readlines()
        fileData.close()

        ####### Khởi tạo Map #######
        nSensors = int(re.sub('\D', '', FILE_NAME.split('/')[-1]))
        sensors = []

        Constant.DEFAULT_X, Constant.DEFAULT_Y = inputData[0].replace('\n', '').split()
        for i in range(1, nSensors + 1):
            xCorr, yCorr, p, E = [float(item) for item in inputData[i].replace('\n', '').split()]
            sensor = Sensor(xCorr, yCorr, p, E)
            sensors.append(sensor)

        wce = WCE()
        map = Map(sensors, wce)

        result = []
        time = np.arange(Constant.T, Constant.DEFAULT_TIME + 1, Constant.T)
        print("[INFO] Database: ", FILE_NAME)
        for t in time:
            print("[INFO] Phase 1 starting ...")
            phase_1()
            print("[INFO] Hành trình tối ưu ...")
            print(Population.individuals[0].path)
            best_path()
            if Map.wce.travelEnergy > Constant.T:
                break
            print("[INFO] Phase 2 starting ...")
            phase_2()
            Population.individuals[0].updateEnergy()
